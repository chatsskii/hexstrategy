using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SkipTurnButtonScript : MonoBehaviour
{

    [SerializeField]
    BattlegroundScript Battleground;

    private Button _skip_turn_button;


    private void Start()
    {
        _skip_turn_button = GetComponent<Button>();
        _skip_turn_button.onClick.AddListener(SkipTurn);

    }

    public void SkipTurn()
    {
        Battleground.EndTurn();
    }


}
