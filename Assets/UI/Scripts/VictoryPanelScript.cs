using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPanelScript : MonoBehaviour
{
    [SerializeField]
    public Text VictoryText;

    void Start()
    {
        Hide();
    }

    public void ShowVictoryText(string text)
    {
        gameObject.SetActive(true);
        VictoryText.text = text;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
