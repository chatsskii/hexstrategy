using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BattlegroundUiScript : MonoBehaviour
{
    [SerializeField]
    public BattlegroundScript Battleground;

    [SerializeField]
    public SkipTurnButtonScript SkipTurnButton;

    [SerializeField]
    public AbilitiesPanelScript AbilitiesPanel;

    [SerializeField]
    public DamageTextScript DamageText;

    [SerializeField]
    public VictoryPanelScript VictoryPanel;
}
