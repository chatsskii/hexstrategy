using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DamageTextScript : MonoBehaviour
{
    [SerializeField]
    BattlegroundScript Battleground;

    private Text _damage_text;

    private void Start()
    {
        _damage_text = GetComponent<Text>();
    }


    public void UpdateDamageText(IDamagableAbility ability, HexTileScript target)
    {

        int max_damage = ability.CalculateMaxDamage(target, true);

        if (max_damage <= 0)
        {
            _damage_text.text = string.Empty;
            return;
        }

        int min_damage = ability.CalculateMinDamage(target, true);
        int min_kills = ability.CalculateMinKills(target, true);
        int max_kills = ability.CalculateMaxKills(target, true);



        string damage_text = string.Format("Damage: {0}-{1} ({2}-{3})", min_kills, max_kills, min_damage, max_damage);
        _damage_text.text = damage_text;


    }
}
