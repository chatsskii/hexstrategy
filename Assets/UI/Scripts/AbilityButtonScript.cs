using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AbilityButtonScript : MonoBehaviour
{
    public BattlegroundScript Battleground;


    private Button _ability_button;

    private BattleAbility _inner_ability = null;

    private void Start()
    {
        _ability_button = GetComponent<Button>();
        _ability_button.onClick.AddListener(UseAbility);
    }

    public void UseAbility()
    {
        if (_inner_ability == null)
            throw new System.Exception("Ability is NULL");

        if (_inner_ability is ITargetableAbility)
        {
      
            Battleground.Control.SetTargetAbility(_inner_ability as ITargetableAbility);
            Debug.LogWarning("2 :" + (Battleground.Control.TargetAbility as BattleAbility).Id);
        }
            
    }


    public void Initialize(BattleAbility ability)
    {
        _inner_ability = ability;
        Battleground = ability.Handler.Battleground;
        GetComponentInChildren<Text>().text = _inner_ability.Id;
    }

}
