using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AbilitiesPanelScript : MonoBehaviour
{
    [SerializeField]
    public AbilityButtonScript AbilityButtonPrefab;


    public AbilityButtonScript[] GetContent()
    {
        return gameObject.GetComponentsInChildren<AbilityButtonScript>();
    }

    public void AddAbilityButton(BattleAbility ability)
    {
        AbilityButtonScript button = Instantiate(AbilityButtonPrefab, transform);
        button.Initialize(ability);
    }

    public void GetGroupAbility(UnitsGroupScript group)
    {
        ClearContent();

        foreach (BattleAbility ability in group.Abilities)
            if (ability is ITargetableAbility)
                AddAbilityButton(ability);
    }


    public void ClearContent()
    {

        List<GameObject> childrens = new List<GameObject>();

        foreach (Transform child in transform)
            childrens.Add(child.gameObject);

        foreach (GameObject child in childrens)
            Destroy(child);
    }


}
