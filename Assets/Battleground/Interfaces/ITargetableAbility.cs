﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public interface ITargetableAbility
{
    bool[][] AvailableTargets { get; }
    void Activate(HexTileScript target);

    List<HexTileScript> Selection(HexTileScript target);

    IEnumerator UpdateAvailableTargets();
}

