﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IDamagableAbility
{

    float DamageModifier { get; }

    int CalculateMinDamage(HexTileScript target, bool is_clamped);
    int CalculateMaxDamage(HexTileScript target, bool is_clamped);

    int CalculateMinKills(HexTileScript target, bool is_clamped);
    int CalculateMaxKills(HexTileScript target, bool is_clamped);

    void SetDamageModifier(float modifier);
}

