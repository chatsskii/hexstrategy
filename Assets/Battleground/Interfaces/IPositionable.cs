using UnityEngine;

public interface IPositionable
{
    Vector2Int Position { get; }
}
