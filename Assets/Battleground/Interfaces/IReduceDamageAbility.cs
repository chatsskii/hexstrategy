﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IModifieDamageAbility
{
    float DamageModifier(int damage, UnitsGroupScript source);

    float MinDamageModifier(int damage, UnitsGroupScript source);
    float MaxDamageModifier(int damage, UnitsGroupScript source);
}

