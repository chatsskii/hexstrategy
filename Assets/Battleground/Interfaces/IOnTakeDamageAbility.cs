﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IOnTakeDamageAbility
{
    void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source);
}

