﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class BattleUnit
{
    private string _id;
    private int _max_health;
    private int _min_damage;
    private int _damage_range;
    private int _initiative;
    private int _move_speed;

    private List<BattleAbility> _abilities = new List<BattleAbility>();

    public string Id { get { return _id; } }
    public int MaxHealth { get { return _max_health; } }
    public int MinDamage { get { return _min_damage; } }
    public int MaxDamage { get { return _min_damage + _damage_range; } }
    public int DamageRange { get { return _damage_range; } }
    public int Initiative { get { return _initiative; } }
    public int MoveSpeed { get { return _move_speed; } }

    public List<BattleAbility> Abilities { get { return _abilities; } }

    public BattleUnit(string id, int max_health)
    {
        _id = id;
        _max_health = max_health;
        _min_damage = 0;
        _damage_range = 0;
        _initiative = 0;
        _move_speed = 0;
    }

    public void SetMaxHealth(int max_health) { _max_health = max_health; }
    public void SetDamage(int min_damage, int damage_range)
    {
        _min_damage = min_damage;
        _damage_range = damage_range;
    }
    public void SetInitiative(int initiative)
    {
        _initiative = initiative;
    }
    public void SetSpeed(int speed)
    {
        _move_speed = speed;
    }
    private void AddAbility(BattleAbility ability)
    {
        _abilities.Add(ability);
    }


    public static BattleUnit Knigth_Unit()
    {
        BattleUnit unit = new BattleUnit("U_KNIGTH", 20);
            unit.SetDamage(6, 4);
            unit.SetInitiative(4);
            unit.SetSpeed(5);
            unit.AddAbility(new Dash_Ability());
            unit.AddAbility(new TemporaryProtection_Ability());

        return unit;
    }

    public static BattleUnit Skeleton_Unit()
    {
        BattleUnit unit = new BattleUnit("U_SKELETON", 6);
            unit.SetDamage(6, 0);
            unit.SetInitiative(6);
            unit.SetSpeed(3);
            unit.AddAbility(new Defragmentation_Ability());

        return unit;
    }

    public static BattleUnit Archer_Unit()
    {
        BattleUnit unit = new BattleUnit("U_ARCHER", 10);
            unit.SetDamage(1, 5);
            unit.SetInitiative(6);
            unit.SetSpeed(6);
            unit.AddAbility(new Shoot_Ability(6, 9));
            unit.AddAbility(new ArrowsRain_Abilitiy());

        return unit;
    }

    public static BattleUnit Zombie_Unit()
    {
        BattleUnit unit = new BattleUnit("U_ZOMBIE", 3000);
            unit.SetDamage(0, 0);
            unit.SetInitiative(4);
            unit.SetSpeed(2);
            unit.AddAbility(new Zombify_Ability());

        return unit;
    }

    public static BattleUnit Ghoul_Unit()
    {
        BattleUnit unit = new BattleUnit("U_GHOUL", 30);
            unit.SetDamage(1, 10);
            unit.SetInitiative(7);
            unit.SetSpeed(5);
            unit.AddAbility(new EatCorpses_Ability());

        return unit;
    }

    public BattleUnit Clone()
    {
        BattleUnit clone = new BattleUnit(_id, _max_health);
            clone._min_damage = _min_damage;
            clone._damage_range = _damage_range;
            clone._initiative = _initiative;
            clone._move_speed = _move_speed;

        return clone;
    }

}

