using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsGroup
{
    private Vector2Int _position;

    private int _units_count;
    private int _health;
    private BattleTeam _team;
    private BattleUnit _source_unit;

    public UnitsGroup(int count, BattleUnit unit, BattleTeam team)
    {
        _units_count = count;
        _team = team;
        _source_unit = unit;
        _health = unit.MaxHealth;
    }

    public Vector2Int Position { get { return _position; } }
    public int Count { get { return _units_count; } }
    public int Health { get { return _health; } }
    public BattleTeam Team { get { return _team; } }
    public BattleUnit Unit { get { return _source_unit; } }

    public void SetPosition(Vector2Int position) { _position = position; }
    public void SetCount(int count)
    {
        if (count < 0)
            _units_count = 0;
        else
            _units_count = count;
    }
    public void SetHealth(int health)
    {
        if (health < 0)
            _health = 0;
        else
            _health = health;
    }
    public void SetTeam(BattleTeam team) { _team = team; }


}
