﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class Zombify_Ability : BattleAbility, IOnTakeDamageAbility
{

    public Zombify_Ability()
    {
        _id = "BA_ZOMBIFY";
        _end_turn = false;
    }


    public void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source)
    {
        if(source == Handler)
        {
            Debug.LogWarning("Zombify");

            Zombified_Effect effect = new Zombified_Effect(Handler, 1);
            target.AddEffect(effect);
        }
    }

    public override BattleAbility Clone()
    {
        return new Zombify_Ability();
    }
}

