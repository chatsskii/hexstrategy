﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class TemporaryProtection_Ability : BattleAbility, IModifieDamageAbility, IOnTakeDamageAbility
{
    private int _charges = 3;

    public TemporaryProtection_Ability()
    {
        _id = "BA_TEMPORARY_PROTECTION";
        _end_turn = false;
    }

    public float DamageModifier(int damage, UnitsGroupScript source)
    {
        if (_charges > 0) return 1 - (_charges * 0.33f);

        return 1;
    }

    public float MaxDamageModifier(int damage, UnitsGroupScript source)
    {
        return DamageModifier(damage, source);
    }

    public float MinDamageModifier(int damage, UnitsGroupScript source)
    {
        return DamageModifier(damage, source);
    }

    public override BattleAbility Clone()
    {
        TemporaryProtection_Ability ability = new TemporaryProtection_Ability();
        ability._charges = _charges;

        return ability;
    }

    public void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source)
    {
        if(target == Handler)
            if (_charges > 0)
                _charges--;
    }
}

