﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

public class EatCorpses_Ability : BattleAbility, ITargetableAbility
{

    private bool[][] _available_targets;

    public EatCorpses_Ability()
    {
        _id = "BA_EAT_CORPSES";
        _end_turn = true;
    }

    public bool[][] AvailableTargets { get { return _available_targets; } }

    public void Activate(HexTileScript target)
    {
        BattlegroundScript battleground = Handler.Battleground;

        foreach (UnitsGroupScript group in new List<UnitsGroupScript>(battleground.AllGroups))
            if (group.Source.Count <= 0 && target.Position.x == group.Position.x && target.Position.y == group.Position.y)
            {
                battleground.DestroyUnitsGroup(group);

                BattleUnit unit = Handler.Source.Unit;

                int max_health = Mathf.RoundToInt(unit.MaxHealth * 1.2f);
                int damage_range = Mathf.RoundToInt(unit.DamageRange * 1.5f);

                unit.SetMaxHealth(max_health);
                unit.SetDamage(unit.MinDamage, damage_range);
                Handler.SetHealth(unit.MaxHealth);

                break;
            }

        if (IsEndTurn) Handler.Battleground.EndTurn();
    }

    public List<HexTileScript> Selection(HexTileScript target)
    {
        if(!IsCorpsesNear)
        {
            BattlegroundScript battleground = Handler.Battleground;
            List<HexTileScript> selection = new List<HexTileScript>();

            foreach (UnitsGroupScript group in battleground.AllGroups)
                if(group.Source.Count <= 0)
                {
                    selection.Add(group.Tile);
                    foreach (HexTileScript tile in battleground.ConnectedTiles(group.Tile))
                        if (tile != null)
                            selection.Add(tile);

                    
                }

            return selection;
        }

        return new List<HexTileScript>() { target };
    }

    public IEnumerator UpdateAvailableTargets()
    {
        BattlegroundScript battleground = Handler.Battleground;

        bool[][] available = new bool[battleground.Tiles.Length][];

        for(int x = 0; x < available.Length; x++)
        {
            available[x] = new bool[battleground.Tiles[x].Length];
            for (int y = 0; y < available[x].Length; y++)
                available[x][y] = false;

            yield return null;
        }


        foreach (UnitsGroupScript group in battleground.AllGroups)
            if (group.Source.Count <= 0 && Vector2Int.Distance(Handler.Position, group.Position) <= 1)
                available[group.Position.x][group.Position.y] = true;        

        _available_targets = available;
        yield return null;
    }

    private bool IsCorpsesNear
    {
        get
        {
            BattlegroundScript battleground = Handler.Battleground;

            foreach (UnitsGroupScript group in battleground.AllGroups)
                if (group.Source.Count <= 0 && Vector2Int.Distance(Handler.Position, group.Position) <= 1)
                    return true;

            return false;
        }
    }

    public override BattleAbility Clone()
    {
        return new EatCorpses_Ability();
    }
}

