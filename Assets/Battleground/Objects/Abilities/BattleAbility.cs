﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class BattleAbility
{
    protected string _id;
    protected bool _end_turn = true;

    private UnitsGroupScript _handler = null;

    public void SetHandler(UnitsGroupScript handler)
    {
        _handler = handler;
    }

    public UnitsGroupScript Handler { get { return _handler; } }
    public bool IsEndTurn { get { return _end_turn; } }
    public string Id { get { return _id; } }


    public abstract BattleAbility Clone();
}

