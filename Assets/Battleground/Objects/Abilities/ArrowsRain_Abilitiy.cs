﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ArrowsRain_Abilitiy : BattleAbility, ITargetableAbility, IDamagableAbility
{

    private bool[][] _avaibale_targets;
    private float _damage_modifier = 1;

    public ArrowsRain_Abilitiy()
    {
        _id = "BA_ARROWS_RAIN";
        _end_turn = true;
    }


    public bool[][] AvailableTargets { get { return _avaibale_targets; } }

    public float DamageModifier { get { return _damage_modifier; } }

    public void Activate(HexTileScript target)
    {
        List<HexTileScript> pattern = SelectionPattern(target);

        foreach (HexTileScript tile in pattern)
            if (tile.IsOccupied && tile.OccupiedObject is UnitsGroupScript)
            {
                foreach (BattleAbility ability in Handler.Abilities)
                    if (ability is IShootAbility)
                    {
                        if(ability is IDamagableAbility)
                        {
                            IDamagableAbility damage_ability = ability as IDamagableAbility;
                            float damage_modifier = damage_ability.DamageModifier * DamageModifier * 0.5f;
                            damage_ability.SetDamageModifier(damage_modifier);
                        }

                        (ability as IShootAbility).Shoot(tile.OccupiedObject as UnitsGroupScript);
                    }
            }

        SetDamageModifier(1);

        if (IsEndTurn) Handler.Battleground.EndTurn();
    }

    public List<HexTileScript> Selection(HexTileScript target)
    {
        return SelectionPattern(target);
    }

    private List<HexTileScript> SelectionPattern(HexTileScript target)
    {
        List<HexTileScript> pattern = new List<HexTileScript> { target };

        BattlegroundScript battleground = Handler.Battleground;

        Vector2Int[] offsets =
        {
            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(-1, 1) : new Vector2Int(0, 1),
            new Vector2Int(0, 2),
            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(-1, -1) : new Vector2Int(0, -1),
            new Vector2Int(0, -2),

            new Vector2Int(1, 0),
            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(0, 1) : new Vector2Int(1, 1),
            new Vector2Int(1, 2),
            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(0, -1) : new Vector2Int(1, -1),
            new Vector2Int(1, -2),

            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(1, 1) : new Vector2Int(2, 1),
            (target.Position.y % 2 == 0 || target.Position.x - Handler.Position.x < 0)? new Vector2Int(1, -1) : new Vector2Int(2, -1),
        };

        foreach (Vector2Int offset in offsets)
        {
            int position_x = target.Position.x + ((target.Position.x - Handler.Position.x > 0)? offset.x : -offset.x);
            int position_y = target.Position.y + offset.y;

            if (position_x >= 0 && position_x < battleground.Tiles.Length && position_y >= 0 && position_y < battleground.Tiles[position_x].Length)
                pattern.Add(battleground.Tiles[position_x][position_y]);
        }

        return pattern;

    }

    public IEnumerator UpdateAvailableTargets()
    {
        BattlegroundScript battleground = Handler.Battleground;

        _avaibale_targets = new bool[battleground.Tiles.Length][];
        for(int x = 0; x < _avaibale_targets.Length; x++)
        {
            _avaibale_targets[x] = new bool[battleground.Tiles[x].Length];

            for(int y = 0; y < _avaibale_targets[x].Length; y++)
            {
                _avaibale_targets[x][y] = battleground.Tiles[x][y].OccupiedObject != Handler;
            }
        }

        yield return null;
    }


    public override BattleAbility Clone()
    {
        return new ArrowsRain_Abilitiy();
    }

    public int CalculateMinDamage(HexTileScript target, bool is_clamped)
    {
        List<HexTileScript> pattern = SelectionPattern(target);

        int damage = 0;

        foreach (HexTileScript tile in pattern)
            if (tile.IsOccupied && tile.OccupiedObject is UnitsGroupScript)
            {
                damage += MinDamageToGroup(tile.OccupiedObject as UnitsGroupScript, is_clamped);
            }

        return damage;
    }

    private int MinDamageToGroup(UnitsGroupScript group, bool is_clamped)
    {
        int damage = 0;

        foreach (BattleAbility ability in Handler.Abilities)
            if (ability is IShootAbility)
            {
                if (ability is IDamagableAbility)
                {
                    IDamagableAbility damage_ability = ability as IDamagableAbility;
                    damage += Mathf.RoundToInt(damage_ability.CalculateMinDamage(group.Tile, false) * DamageModifier * 0.5f);
                }

            }

        int max_health = group.Source.Health + (group.Source.Count - 1) * group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return damage;
        else
            return Mathf.Min(max_health, damage);
    }

    public int CalculateMaxDamage(HexTileScript target, bool is_clamped)
    {
        List<HexTileScript> pattern = SelectionPattern(target);

        int damage = 0;

        foreach (HexTileScript tile in pattern)
            if (tile.IsOccupied && tile.OccupiedObject is UnitsGroupScript)
            {
                damage += MaxDamageToGroup(tile.OccupiedObject as UnitsGroupScript, is_clamped);
            }

        return damage;
    }

    private int MaxDamageToGroup(UnitsGroupScript group, bool is_clamped)
    {
        int damage = 0;

        foreach (BattleAbility ability in Handler.Abilities)
            if (ability is IShootAbility)
            {
                if (ability is IDamagableAbility)
                {
                    IDamagableAbility damage_ability = ability as IDamagableAbility;
                    damage += Mathf.RoundToInt(damage_ability.CalculateMaxDamage(group.Tile, false) * DamageModifier * 0.5f);
                }

            }

        int max_health = group.Source.Health + (group.Source.Count - 1) * group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return damage;
        else
            return Mathf.Min(max_health, damage);
    }

    public int CalculateMinKills(HexTileScript target, bool is_clamped)
    {
        List<HexTileScript> pattern = SelectionPattern(target);

        int kills = 0;

        foreach (HexTileScript tile in pattern)
            if (tile.IsOccupied && tile.OccupiedObject is UnitsGroupScript)
            {
                kills += MinKillsToGroup(tile.OccupiedObject as UnitsGroupScript, is_clamped);
            }

        return kills;
    }

    private int MinKillsToGroup(UnitsGroupScript group, bool is_clamped)
    {
        int kills = MinDamageToGroup(group, false) / group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return kills;
        else
            return Mathf.Min(group.Source.Count, kills);
    }

    public int CalculateMaxKills(HexTileScript target, bool is_clamped)
    {
        List<HexTileScript> pattern = SelectionPattern(target);

        int kills = 0;

        foreach (HexTileScript tile in pattern)
            if (tile.IsOccupied && tile.OccupiedObject is UnitsGroupScript)
            {
                kills += MaxKillsToGroup(tile.OccupiedObject as UnitsGroupScript, is_clamped);
            }

        return kills;
    }

    private int MaxKillsToGroup(UnitsGroupScript group, bool is_clamped)
    {
        int kills = MaxDamageToGroup(group, false) / group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return kills;
        else
            return Mathf.Min(group.Source.Count, kills);
    }

    public void SetDamageModifier(float modifier)
    {
        _damage_modifier = modifier;
    }
}

