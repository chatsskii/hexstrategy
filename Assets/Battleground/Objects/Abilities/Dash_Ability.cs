﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Dash_Ability : BattleAbility, IOnStartTurnAbility
{

    private static float ACTIVATION_CHANCE = 0.4f;

    public Dash_Ability()
    {
        _id = "BA_DASH";
        _end_turn = false;
    }

    public void OnStartTurn(UnitsGroupScript group_turn)
    {
        if(group_turn == Handler)
        {
            bool activate = Random.Range(0f, 1f) <= ACTIVATION_CHANCE;

            if(activate)
            {
                BattlegroundScript battleground = Handler.Battleground;
                battleground.AddToTurnsQueue(Handler, 0);
            }

        }
    }

    public override BattleAbility Clone()
    {
        return new Dash_Ability();
    }
}

