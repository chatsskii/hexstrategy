﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class MoveOrAttack_Ability : BattleAbility, ITargetableAbility, IDamagableAbility
{

    private bool[][] _avaibale_targets;
    private float _damage_modifier = 1;

    public MoveOrAttack_Ability()
    {
        _id = "BA_MOVE_OR_ATTACK";
        _end_turn = true;
    }

    public bool[][] AvailableTargets
    {
        get
        {
            return _avaibale_targets;
        }
    }

    public float DamageModifier { get { return _damage_modifier = 1; } }

    public void Activate(HexTileScript target)
    {
        BattlegroundScript battleground = Handler.Battleground;

        HexTileScript move_position = target;

        if (target.IsOccupied)
        {
            List<HexTileScript> path = battleground.FindPath(Handler.Tile, target, Handler.Source.Unit.MoveSpeed + 1);

            

            if (Handler.IsShooting && Vector2Int.Distance(target.Position, Handler.Position) > 1)
                Shoot(target.OccupiedObject as UnitsGroupScript);
            else
            {
                move_position = path[path.Count - 2];
                Handler.SetPosition(move_position);
                Handler.transform.position = battleground.GetSpacePosition(move_position.Position);

                Attack(target.OccupiedObject as UnitsGroupScript);
            }
                

        }
        else
        {
            Handler.SetPosition(move_position);
            Handler.transform.position = battleground.GetSpacePosition(move_position.Position);
        }



        if (IsEndTurn)
            Handler.Battleground.EndTurn();
    }

    public List<HexTileScript> Selection(HexTileScript target)
    {
        if (!_avaibale_targets[target.Position.x][target.Position.y]) return new List<HexTileScript>();

        if (Handler.IsShooting && target.IsOccupied)
            return new List<HexTileScript>() { target };

        BattlegroundScript battleground = Handler.Battleground;
        HexTileScript start = battleground.Tiles[Handler.Position.x][Handler.Position.y];
        HexTileScript end = target;

        List<HexTileScript> path = battleground.FindPath(start, end, Handler.Source.Unit.MoveSpeed + 2);
        return path;
    }

    public IEnumerator UpdateAvailableTargets()
    {
        BattlegroundScript battleground = Handler.Battleground;


        bool[][] available = new bool[battleground.Tiles.Length][];

        for (int x = 0; x < available.Length; x++)
        {
            available[x] = new bool[battleground.Tiles[x].Length];

            for(int y = 0; y < available[x].Length; y++)
            {
                HexTileScript tile = battleground.Tiles[x][y];
                if (tile.IsOccupied)
                {
                    if(tile.OccupiedObject is UnitsGroupScript)
                        if((tile.OccupiedObject as UnitsGroupScript).Source.Team != Handler.Source.Team)
                        {
                            if (Handler.IsShooting)
                                available[x][y] = true;
                            else
                                available[x][y] = battleground.FindPath(Handler.Tile, tile, Handler.Source.Unit.MoveSpeed + 1) != null;
                        }
                            
                }
                else
                    available[x][y] = battleground.FindPath(Handler.Tile, tile, Handler.Source.Unit.MoveSpeed) != null;

                yield return null;
            }
        }

        _avaibale_targets = available;
        yield return null;
    }

    private void Attack(UnitsGroupScript target)
    {
        int damage = 0;

        for (int i = 0; i < Handler.Source.Count; i++)
            damage += Handler.Source.Unit.MinDamage + Random.Range(0, Handler.Source.Unit.DamageRange);

        Debug.LogWarning("Damage: " + damage);

        damage = Mathf.RoundToInt(damage * DamageModifier * target.DamageModifier(damage, Handler));
        target.TakeDamage(damage, Handler);

        SetDamageModifier(1);
    }

    private void Shoot(UnitsGroupScript target)
    {
        foreach(BattleAbility ability in Handler.Abilities)
            if (ability is IShootAbility)
                (ability as IShootAbility).Shoot(target);

    }

    public int CalculateMinDamage(HexTileScript target, bool is_clamped)
    {
        if (!_avaibale_targets[target.Position.x][target.Position.y]) return 0;

        if (target.IsOccupied)
        {

            int damage = 0;

            if (Handler.IsShooting && Vector2Int.Distance(target.Position, Handler.Position) > 1)
            {

                foreach (BattleAbility shoot_ability in Handler.Abilities)
                    if (shoot_ability is IShootAbility && shoot_ability is IDamagableAbility)
                        damage += (shoot_ability as IDamagableAbility).CalculateMinDamage(target, false);
            }
            else
            {
                damage = Mathf.RoundToInt(Handler.Source.Unit.MinDamage * Handler.Source.Count * DamageModifier);                
            }


            UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
            damage = Mathf.RoundToInt(damage * target_group.CalculateMinDamageModifier(damage, Handler));

            int max_health = target_group.Source.Health + (target_group.Source.Count - 1) * target_group.Source.Unit.MaxHealth;

            if (!is_clamped)
                return damage;
            else
                return Mathf.Min(damage, max_health);


        }

        return 0;

    }

    public int CalculateMaxDamage(HexTileScript target, bool is_clamped)
    {
        if (!_avaibale_targets[target.Position.x][target.Position.y]) return 0;

        if (target.IsOccupied)
        {

            int damage = 0;

            if (Handler.IsShooting && Vector2Int.Distance(target.Position, Handler.Position) > 1)
            {

                foreach (BattleAbility shoot_ability in Handler.Abilities)
                    if (shoot_ability is IShootAbility && shoot_ability is IDamagableAbility)
                        damage += (shoot_ability as IDamagableAbility).CalculateMaxDamage(target, false);
            }
            else
            {
                damage = Mathf.RoundToInt(Handler.Source.Unit.MaxDamage * Handler.Source.Count * DamageModifier);
            }


            UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
            damage = Mathf.RoundToInt(damage * target_group.CalculateMaxDamageModifier(damage, Handler));

            int max_health = target_group.Source.Health + (target_group.Source.Count - 1) * target_group.Source.Unit.MaxHealth;

            if (!is_clamped)
                return damage;
            else
                return Mathf.Min(damage, max_health);


        }

        return 0;
    }

    public int CalculateMinKills(HexTileScript target, bool is_clamped)
    {
        UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
        int min_kills = CalculateMinDamage(target, false) / target_group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return min_kills;
        else
            return Mathf.Min(target_group.Source.Count, min_kills);
    }

    public int CalculateMaxKills(HexTileScript target, bool is_clamped)
    {
        UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
        int max_kills = CalculateMaxDamage(target, false) / target_group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return max_kills;
        else
            return Mathf.Min(target_group.Source.Count, max_kills);
    }

    public void SetDamageModifier(float modifier)
    {
        _damage_modifier = modifier;
    }

    public override BattleAbility Clone()
    {
        return new MoveOrAttack_Ability();
    }
}

