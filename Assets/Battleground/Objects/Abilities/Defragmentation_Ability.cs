﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Defragmentation_Ability : BattleAbility, IOnTakeDamageAbility
{
    private int _charges = 3;

    public Defragmentation_Ability()
    {
        _id = "BA_DEFRAGMENTATION";
        _end_turn = false;

        _charges = 3;
    }

    public void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source)
    {
        if (target != Handler) return;

        if (_charges <= 0) return;

        List<HexTileScript> possible_tiles = new List<HexTileScript>();
        BattlegroundScript battleground = Handler.Battleground;


        foreach (HexTileScript tile in battleground.ConnectedTiles(Handler.Tile))
            if (tile != null)
                if (!tile.IsOccupied)
                    possible_tiles.Add(tile);

        if (possible_tiles.Count == 0) return;

        int random_index = Random.Range(0, possible_tiles.Count - 1);

        HexTileScript spawn_tile = possible_tiles[random_index];


        BattleUnit handler_unit = Handler.Source.Unit;

        int kills = (damage - Handler.Source.Unit.MaxHealth) / handler_unit.MaxHealth;

        float stats_modifier = _charges * 0.3f;

        int max_health = Mathf.RoundToInt(handler_unit.MaxHealth * stats_modifier);
        int min_damage = Mathf.RoundToInt(handler_unit.MinDamage * stats_modifier);
        int damage_range = Mathf.RoundToInt(handler_unit.DamageRange * stats_modifier);
        int initiative = Mathf.RoundToInt(handler_unit.Initiative * stats_modifier);
        int move_speed = Mathf.RoundToInt(handler_unit.MoveSpeed * stats_modifier);

        BattleUnit defragmentation_clone = handler_unit.Clone();
            defragmentation_clone.SetMaxHealth(max_health);
            defragmentation_clone.SetDamage(min_damage, damage_range);
            defragmentation_clone.SetInitiative(initiative);
            defragmentation_clone.SetSpeed(move_speed);

        UnitsGroup defragmentation_group = new UnitsGroup(kills, defragmentation_clone, Handler.Source.Team);

        UnitsGroupScript units_script = battleground.SpawnUnitsGroup(spawn_tile, defragmentation_group);

        

        int counter = 0;

        while(counter < units_script.Abilities.Count)
        {
            BattleAbility current_ability = units_script.Abilities[counter];
            if (current_ability is Defragmentation_Ability)
            {
                Defragmentation_Ability cloned_ability = units_script.Abilities[counter] as Defragmentation_Ability;
                units_script.RemoveAbility(cloned_ability);
                continue;
            }
            counter++;
        }


        _charges--;

    }

    public override BattleAbility Clone()
    {
        Defragmentation_Ability clone = new Defragmentation_Ability();
        clone._charges = _charges;

        return clone;
    }

}

