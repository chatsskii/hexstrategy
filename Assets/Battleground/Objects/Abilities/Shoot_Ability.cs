﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class Shoot_Ability : BattleAbility, IShootAbility, IDamagableAbility
{
    private int _min_damage;
    private int _damage_range;

    private float _damage_modifier = 1;

    public float DamageModifier { get { return _damage_modifier; } }

    public Shoot_Ability(int min_damage, int damage_range)
    {
        _id = "BA_SHOOT";

        _min_damage = min_damage;
        _damage_range = damage_range;
    }


    public void Shoot(UnitsGroupScript target)
    {
        int damage = 0;

        for (int i = 0; i < Handler.Source.Count; i++)
            damage += _min_damage + Random.Range(0, _damage_range);

        Debug.LogWarning("Before: " + damage);

        damage = Mathf.RoundToInt(damage * DamageModifier * target.DamageModifier(damage, Handler));

        Debug.LogWarning("After: " + damage);

        target.TakeDamage(damage, Handler);

        SetDamageModifier(1);
    }

    public int CalculateMinDamage(HexTileScript target, bool is_clamped)
    {
        if(target.IsOccupied)
            if(target.OccupiedObject is UnitsGroupScript)
            {
                UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
                int max_health = target_group.Source.Health + (target_group.Source.Count - 1) * target_group.Source.Unit.MaxHealth;
                int min_damage = Mathf.RoundToInt(_min_damage * Handler.Source.Count * DamageModifier);

                min_damage = Mathf.RoundToInt(min_damage * target_group.CalculateMinDamageModifier(min_damage, Handler));

                if (!is_clamped)
                    return min_damage;
                else
                    return Mathf.Min(max_health, min_damage);
            }

        return 0;
    }

    public int CalculateMaxDamage(HexTileScript target, bool is_clamped)
    {
        if (target.IsOccupied)
            if (target.OccupiedObject is UnitsGroupScript)
            {
                UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
                int max_health = target_group.Source.Health + (target_group.Source.Count - 1) * target_group.Source.Unit.MaxHealth;
                int max_damage = Mathf.RoundToInt((_min_damage + _damage_range) * Handler.Source.Count * DamageModifier);

                max_damage = Mathf.RoundToInt(max_damage * target_group.CalculateMaxDamageModifier(max_damage, Handler));

                if (!is_clamped)
                    return max_damage;
                else
                    return Mathf.Min(max_health, max_damage);
            }

        return 0;
    }

    public int CalculateMinKills(HexTileScript target, bool is_clamped)
    {
        UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
        int min_kills = CalculateMinDamage(target, false) / target_group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return min_kills;
        else
            return Mathf.Min(target_group.Source.Count, min_kills);
    }

    public int CalculateMaxKills(HexTileScript target, bool is_clamped)
    {
        UnitsGroupScript target_group = target.OccupiedObject as UnitsGroupScript;
        int max_kills = CalculateMaxDamage(target, false) / target_group.Source.Unit.MaxHealth;

        if (!is_clamped)
            return max_kills;
        else
            return Mathf.Min(target_group.Source.Count, max_kills);
    }

    public void SetDamageModifier(float modifier)
    {
        _damage_modifier = modifier;
    }

    public override BattleAbility Clone()
    {
        return new Shoot_Ability(_min_damage, _damage_range);
    }
}

