﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public abstract class BattleEffect
{
    protected string _id;
    protected UnitsGroupScript _handler;

    public void SetHandler(UnitsGroupScript handler)
    {
        _handler = handler;
    }

    public void RemoveSelf()
    {
        Handler.RemoveEffect(this);
    }

    public UnitsGroupScript Handler { get { return _handler; } }

    public abstract BattleEffect Clone();
}

