﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Zombified_Effect : BattleEffect, ITickableEffect
{
    private int _turns = 1;
    private UnitsGroupScript _inflictor = null;
    private BattleTeam _original_team;

    private bool _is_first_tick = true;

    public Zombified_Effect(UnitsGroupScript inflictor, int turns)
    {
        _id = "E_ZOMBIFIED";
        _is_first_tick = true;
        _turns = turns;
        _inflictor = inflictor;
    }

    public void Tick()
    {
        if (_turns > 0)
        {
            if (_is_first_tick)
            {
                _original_team = Handler.Source.Team;
                _is_first_tick = false;
            }
                

            Handler.SetTeam(_inflictor.Source.Team);
            _turns--;
        }
        else
        {
            Handler.SetTeam(_original_team);
            RemoveSelf();
        }

    }

    public override BattleEffect Clone()
    {
        throw new NotImplementedException();
    }
}

