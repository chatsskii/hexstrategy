using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BattlegroundScript))]
public class BattlegroundControlScript : MonoBehaviour
{
    BattlegroundScript _battleground;
    ITargetableAbility _target_ability = null;

    private bool _input_enabled = false;


    private void Start()
    {
        _battleground = GetComponent<BattlegroundScript>();
        _input_enabled = true;
    }

    private void Update()
    {
        if (!IsEnabled) return;


        if (_target_ability == null) return;

        if (Input.GetMouseButtonDown(1) && !(_target_ability is MoveOrAttack_Ability))
        {
            SetTargetAbility(_battleground.ActiveGroup.MoveAbility);
        }


        HexTileScript selected_hex = GetHexUnderMouse();

        bool[][] available_targets = _target_ability.AvailableTargets;
        if (available_targets == null) return;

        VisulizeSelection(selected_hex);


        if (selected_hex != null)
        {
            if (available_targets[selected_hex.Position.x][selected_hex.Position.y])
            {
                if(_target_ability is IDamagableAbility)
                {
                    _battleground.BattlegroundUi.DamageText.UpdateDamageText(_target_ability as IDamagableAbility, selected_hex);
                }
                    

                if (Input.GetMouseButtonDown(0))
                    _target_ability.Activate(selected_hex);
            }
        }
        
    }

    public void SetTargetAbility(ITargetableAbility ability)
    {
        _target_ability = ability;

        if (_target_ability != null)
           StartCoroutine(_target_ability.UpdateAvailableTargets());
    }

    public void SetEnabled(bool enabled) { _input_enabled = enabled; }


    public bool IsEnabled { get { return _input_enabled; } }

    public ITargetableAbility TargetAbility { get { return _target_ability; } }

    public HexTileScript GetHexUnderMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hit = Physics.RaycastAll(ray);


        foreach(RaycastHit crr_hit in hit)
        {
            HexTileScript hex_tile = crr_hit.collider.gameObject.GetComponent<HexTileScript>();
            if (hex_tile != null) return hex_tile;
        }

        return null;
    }


    private void ClearSelection()
    {
        for (int x = 0; x < _battleground.Tiles.Length; x++)
            for (int y = 0; y < _battleground.Tiles[y].Length; y++)
                _battleground.Tiles[x][y].GetComponent<MeshRenderer>().material.color = Color.white;
    }

    private void VisulizeSelection(HexTileScript selected_hex)
    {
        bool[][] available_targets = _target_ability.AvailableTargets;

        List<HexTileScript> selected_tiles = new List<HexTileScript>();

        if (selected_hex != null)
        {
            selected_tiles = _target_ability.Selection(selected_hex);
        }

        for (int x = 0; x < available_targets.Length; x++)
            for (int y = 0; y < available_targets[x].Length; y++)
            {
                HexTileScript tile = _battleground.Tiles[x][y];

                if(selected_tiles.Contains(tile))
                {
                    tile.GetComponent<MeshRenderer>().material.color = Color.blue;
                    continue;
                }

                if (available_targets[x][y])
                    tile.GetComponent<MeshRenderer>().material.color = Color.green;
                else
                    tile.GetComponent<MeshRenderer>().material.color = Color.red;
            }
    }

}
