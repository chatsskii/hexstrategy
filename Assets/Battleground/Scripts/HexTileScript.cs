using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexTileScript : MonoBehaviour
{
    private Vector2Int _position;
    private IPositionable _occupied_object = null;


    public void Initialize(Vector2Int position)
    {
        _position = position;
    }

    public bool IsOccupied { get { return _occupied_object != null; } }
    public IPositionable OccupiedObject { get { return _occupied_object; } }
    public Vector2Int Position { get { return _position; } }

    public void RemoveObject() { _occupied_object = null; }
    public void SetObject(IPositionable tile_object) { _occupied_object = tile_object; }
}
