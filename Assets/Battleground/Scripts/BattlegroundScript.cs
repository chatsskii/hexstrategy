using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlegroundScript : MonoBehaviour
{
    [SerializeField]
    public int FieldHeigth = 1;

    [SerializeField]
    public int FieldWidth = 1;

    [SerializeField]
    public float TileSize = 1f;

    [SerializeField]
    public BattlegroundUiScript BattlegroundUi;

    [SerializeField]
    public HexTileScript HexTilePrefab;

    [SerializeField]
    public UnitsGroupScript UnitsGroupPrefab;


    private BattlegroundControlScript _control = null;

    private List<UnitsGroupScript> _units_groups = new List<UnitsGroupScript>();
    private HexTileScript[][] _tiles;

    private UnitsGroupScript _active_group = null;
    private List<UnitsGroupScript> _turns_queue = new List<UnitsGroupScript>();

    private void Start()
    {
        _control = GetComponent<BattlegroundControlScript>();

        SpawnHexTiles();

        SpawnArcherGroup(new Vector2Int(0, 0));
        SpawnKnigthGroup(new Vector2Int(1, 1));
        SpawnKnigthGroup(new Vector2Int(0, 1));
        SpawnKnigthGroup(new Vector2Int(1, 0));

        SpawnSkeletonGroup(new Vector2Int(3, 3));
        SpawnSkeletonGroup(new Vector2Int(5, 6));
        SpawnSkeletonGroup(new Vector2Int(3, 4));
        SpawnSkeletonGroup(new Vector2Int(6, 4));

        SpawnZombieGroup(new Vector2Int(4, 4));
        SpawnGhoulGroup(new Vector2Int(6, 7));

        StartCoroutine(TurnsUpdate());
    }

    public UnitsGroupScript ActiveGroup { get { return _active_group; } }
    public List<UnitsGroupScript> TurnsQueue { get { return _turns_queue; } }

    public HexTileScript[][] Tiles { get { return _tiles; } }

    public HexTileScript[] ConnectedTiles(HexTileScript source)
    {
        // ======== Tiles Position =====
        //        5 0
        //       4   1
        //        3 2

        HexTileScript[] connected_tiles = new HexTileScript[6];

        if (source.Position.y % 2 == 0)
        {

            if (source.Position.y <= 0)
                connected_tiles[0] = null;
            else
                connected_tiles[0] = Tiles[source.Position.x][source.Position.y - 1];


            if (source.Position.x >= FieldWidth - 1)
                connected_tiles[1] = null;
            else
                connected_tiles[1] = Tiles[source.Position.x + 1][source.Position.y];


            if (source.Position.y >= FieldHeigth - 1)
                connected_tiles[2] = null;
            else
                connected_tiles[2] = Tiles[source.Position.x][source.Position.y + 1];


            if (source.Position.x <= 0 || source.Position.y >= FieldHeigth - 1)
                connected_tiles[3] = null;
            else
                connected_tiles[3] = Tiles[source.Position.x - 1][source.Position.y + 1];


            if (source.Position.x <= 0)
                connected_tiles[4] = null;
            else
                connected_tiles[4] = Tiles[source.Position.x - 1][source.Position.y];


            if (source.Position.x <= 0 || source.Position.y <= 0)
                connected_tiles[5] = null;
            else
                connected_tiles[5] = Tiles[source.Position.x - 1][source.Position.y - 1];
        }
        else
        {
            if (source.Position.x >= FieldWidth - 1 || source.Position.y <= 0)
                connected_tiles[0] = null;
            else
                connected_tiles[0] = Tiles[source.Position.x + 1][source.Position.y - 1];


            if (source.Position.x >= FieldWidth - 1)
                connected_tiles[1] = null;
            else
                connected_tiles[1] = Tiles[source.Position.x + 1][source.Position.y];


            if (source.Position.x >= FieldWidth - 1 || source.Position.y >= FieldHeigth - 1)
                connected_tiles[2] = null;
            else
                connected_tiles[2] = Tiles[source.Position.x + 1][source.Position.y + 1];


            if (source.Position.y >= FieldHeigth - 1)
                connected_tiles[3] = null;
            else
                connected_tiles[3] = Tiles[source.Position.x][source.Position.y + 1];


            if (source.Position.x <= 0)
                connected_tiles[4] = null;
            else
                connected_tiles[4] = Tiles[source.Position.x - 1][source.Position.y];


            if (source.Position.y <= 0)
                connected_tiles[5] = null;
            else
                connected_tiles[5] = Tiles[source.Position.x][source.Position.y - 1];
        }

        return connected_tiles;
    }


    public BattlegroundControlScript Control { get { return _control; } }

    public UnitsGroupScript SpawnUnitsGroup(HexTileScript tile, UnitsGroup group)
    {
        UnitsGroupScript group_script = Instantiate(UnitsGroupPrefab);
            group_script.Initialize(this, group);
            group_script.SetPosition(tile);

        group_script.transform.position = GetSpacePosition(tile.Position);

        _units_groups.Add(group_script);

        return group_script;
    }

    public void DestroyUnitsGroup(UnitsGroupScript group)
    {
        for (int x = 0; x < Tiles.Length; x++)
            for (int y = 0; y < Tiles[x].Length; y++)
                if (Tiles[x][y].OccupiedObject == group)
                    Tiles[x][y].RemoveObject();


        _turns_queue.Remove(group);
        _units_groups.Remove(group);

        Destroy(group);
    }

    public Vector3 GetSpacePosition(Vector2Int game_position)
    {
        HexTileScript hex_tile = Tiles[game_position.x][game_position.y];

        float x_position = hex_tile.transform.position.x;
        float y_position = hex_tile.transform.position.y;
        float z_position = hex_tile.transform.position.z - 1f;

        return new Vector3(x_position, y_position, z_position);
    }

    private void SpawnHexTiles()
    {
        _tiles = new HexTileScript[FieldWidth][];
        for (int x = 0; x < _tiles.Length; x++)
        {
            _tiles[x] = new HexTileScript[FieldHeigth];
            for (int y = 0; y < _tiles[x].Length; y++)
            {
                HexTileScript crr_tile = Instantiate(HexTilePrefab, transform);
                crr_tile.Initialize(new Vector2Int(x, y));

                float x_offset = (y % 2 == 0) ? 0 : TileSize / 2;
                float x_position = transform.position.x + x_offset + TileSize * (x - FieldWidth / 2);
                float y_position = transform.position.y + TileSize * (y - FieldHeigth / 2);
                float z_poistion = transform.position.z;

                crr_tile.transform.position = new Vector3(x_position, y_position, z_poistion);
                _tiles[x][y] = crr_tile;
            }
        }
    }

    private void SpawnKnigthGroup(Vector2Int position)
    {
        BattleUnit knigth_unit = BattleUnit.Knigth_Unit();
        UnitsGroup knigth_group = new UnitsGroup(20, knigth_unit, BattleTeam.Player1);

        HexTileScript tile = Tiles[position.x][position.y];
        SpawnUnitsGroup(tile, knigth_group);
    }

    private void SpawnSkeletonGroup(Vector2Int position)
    {
        BattleUnit skeleton_unit = BattleUnit.Skeleton_Unit();
        UnitsGroup skeleton_group = new UnitsGroup(50, skeleton_unit, BattleTeam.Player2);

        HexTileScript tile = Tiles[position.x][position.y];
        SpawnUnitsGroup(tile, skeleton_group);
    }

    private void SpawnArcherGroup(Vector2Int position)
    {
        BattleUnit archer_unit = BattleUnit.Archer_Unit();
        UnitsGroup archer_group = new UnitsGroup(40, archer_unit, BattleTeam.Player1);

        HexTileScript tile = Tiles[position.x][position.y];
        SpawnUnitsGroup(tile, archer_group);
    }

    private void SpawnZombieGroup(Vector2Int position)
    {
        BattleUnit zombie_unit = BattleUnit.Zombie_Unit();
        UnitsGroup zombie_group = new UnitsGroup(1, zombie_unit, BattleTeam.Player2);

        HexTileScript tile = Tiles[position.x][position.y];
        SpawnUnitsGroup(tile, zombie_group);
    }

    private void SpawnGhoulGroup(Vector2Int position)
    {
        BattleUnit ghoul_unit = BattleUnit.Ghoul_Unit();
        UnitsGroup ghoul_group = new UnitsGroup(20, ghoul_unit, BattleTeam.Player2);

        HexTileScript tile = Tiles[position.x][position.y];
        SpawnUnitsGroup(tile, ghoul_group);
    }

    public void AddToTurnsQueue(UnitsGroupScript group, int position)
    {
        _turns_queue.Insert(position, group);
    }

    public List<UnitsGroupScript> AllGroups { get { return _units_groups; } }


    public bool IsTeamActive(BattleTeam team)
    {
        foreach (UnitsGroupScript group in AllGroups)
            if (group.Source.Team == team && group.Source.Count > 0)
                return true;

        return false;
    }

    private List<UnitsGroupScript> GetTurnQueue()
    {
        List<UnitsGroupScript> groups = new List<UnitsGroupScript>(AllGroups);

        groups.Sort((UnitsGroupScript group1, UnitsGroupScript group2) => { return group2.Source.Unit.Initiative - group1.Source.Unit.Initiative; });

        return groups;
    }

    private IEnumerator TurnsUpdate()
    {
        while(IsTeamActive(BattleTeam.Player1) && IsTeamActive(BattleTeam.Player2))
        {
            _turns_queue = GetTurnQueue();
            while(_turns_queue.Count > 0)
            {

                if(_active_group == null)
                {

                    if (_turns_queue[0].Source.Count > 0)
                    {
                        StartTurn(_turns_queue[0]);
                    }
                        
                    if(_turns_queue.Count > 0)
                        _turns_queue.RemoveAt(0);

                }

                yield return null;
            }
        }

        ShowVictoryText();

        yield return null;
    }


    public void OnStartTurn(UnitsGroupScript group_turn)
    {
        List<UnitsGroupScript> triggered_groups = new List<UnitsGroupScript>(AllGroups);

        foreach (UnitsGroupScript group in triggered_groups)
            group.OnStartTurn(group_turn);
    }

    public void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source)
    {
        List<UnitsGroupScript> triggered_groups = new List<UnitsGroupScript>(AllGroups);

        foreach (UnitsGroupScript group in triggered_groups)
            group.OnTakeDamage(damage, target, source);
    }

    

    public void StartTurn(UnitsGroupScript group)
    {
        _active_group = group;
        Control.SetTargetAbility(group.MoveAbility);

        group.TickEffects();
        OnStartTurn(group);

        BattlegroundUi.AbilitiesPanel.GetGroupAbility(group);
    }

    public void EndTurn()
    {
        _active_group = null;
        Control.SetTargetAbility(null);
    }

    public List<HexTileScript> FindPath(HexTileScript start, HexTileScript end, int max_distance)
    {
        try
        {

            if (Mathf.Abs(start.Position.x - end.Position.x) > max_distance || Mathf.Abs(start.Position.y - end.Position.y) > max_distance)
                return null;

            List<PathData> visited = new List<PathData>();
            List<PathData> pool = new List<PathData>() { new PathData(start, null) };



        while (pool.Count > 0)
            {

                PathData current_hex = pool[0];
                float min_priority = current_hex.Priority(end);

                foreach (PathData hex in pool)
                {
                    float priority = hex.Priority(end);
                    if (priority < min_priority)
                    {
                        current_hex = hex;
                        min_priority = priority;
                    }
                }

                pool.Remove(current_hex);

                //Debug.LogWarning("Current: [" + current_hex.Tile.Position.x + "," + current_hex.Tile.Position.y + "]");

                if (current_hex.Tile == end)
                {
                    PathData adding_tile = current_hex;
                    List<HexTileScript> path = new List<HexTileScript>();

                    while (adding_tile != null)
                    {
                        path.Insert(0, adding_tile.Tile);
                        adding_tile = adding_tile.Previous;
                    }

                    return path;
                }


                PathData previous_path = visited.Find((PathData hex) => { return hex.Tile == current_hex.Tile; });
                if (previous_path != null)
                {
                    if (current_hex.Distance < previous_path.Distance)
                    {
                        visited.Remove(previous_path);
                        visited.Add(current_hex);
                        if (current_hex.Distance == max_distance) continue;

                        foreach (HexTileScript connected_tile in ConnectedTiles(current_hex.Tile))
                            if (connected_tile != null)
                                if (!connected_tile.IsOccupied || connected_tile == end)
                                    pool.Add(new PathData(connected_tile, current_hex));
                    }
                }
                else
                {
                    visited.Add(current_hex);
                    if (current_hex.Distance == max_distance) continue;

                    foreach (HexTileScript connected_tile in ConnectedTiles(current_hex.Tile))
                        if (connected_tile != null)
                            if (!connected_tile.IsOccupied || connected_tile == end)
                                pool.Add(new PathData(connected_tile, current_hex));
                }

            }

        }
        catch
        {

        }


        return null;
    }

    private class PathData
    {
        public HexTileScript Tile;
        public PathData Previous;
        public int Distance;

        public PathData(HexTileScript tile, PathData previous)
        {
            Tile = tile;
            Previous = previous;

            if (previous != null)
                Distance = previous.Distance + 1;
            else
                Distance = 0;
        }


        public float Priority(HexTileScript target)
        {
            return 0.6f * Distance + 0.4f *(Mathf.Abs(target.Position.x - Tile.Position.x) + Mathf.Abs(target.Position.y - Tile.Position.y));
        }
    }


    public void ShowVictoryText()
    {
        string victory_text;

        if(IsTeamActive(BattleTeam.Player1))
        {
            victory_text = "Player1 Win !";
        }
        else
        {
            victory_text = "Player2 Win !";
        }

        BattlegroundUi.VictoryPanel.ShowVictoryText(victory_text);

    }
}
