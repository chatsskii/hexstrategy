using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitsGroupScript : MonoBehaviour, IPositionable
{
    [SerializeField]
    public TextMesh Counter;

    [SerializeField]
    public TextMesh Name;

    private BattlegroundScript _battleground;
    private UnitsGroup _source_group;

    private List<BattleAbility> _abilities = new List<BattleAbility>();
    private List<BattleEffect> _effects = new List<BattleEffect>();

    public UnitsGroup Source { get { return _source_group; } }
    public BattlegroundScript Battleground { get { return _battleground; } }

    public Vector2Int Position { get { return _source_group.Position; } }
    public HexTileScript Tile { get { return Battleground.Tiles[Position.x][Position.y]; } }

    public List<BattleAbility> Abilities { get { return _abilities; } }
    public MoveOrAttack_Ability MoveAbility
    {
        get
        {
            MoveOrAttack_Ability ability = new MoveOrAttack_Ability();
            ability.SetHandler(this);

            return ability;
        }
    }

    public List<BattleEffect> Effects { get { return _effects; } }
    public void AddEffect(BattleEffect effect)
    {
        effect.SetHandler(this);
        _effects.Add(effect);
    }
    public void RemoveEffect(BattleEffect effect)
    {
        _effects.Remove(effect);
    }

    public float CalculateMinDamageModifier(int damage, UnitsGroupScript source)
    {
        float damage_modifier = 1;

        foreach (BattleAbility ability in Abilities)
            if (ability is IModifieDamageAbility)
                damage_modifier *= (ability as IModifieDamageAbility).MinDamageModifier(damage, source);

        return damage_modifier;
    }

    public float CalculateMaxDamageModifier(int damage, UnitsGroupScript source)
    {
        float damage_modifier = 1;

        foreach (BattleAbility ability in Abilities)
            if (ability is IModifieDamageAbility)
                damage_modifier *= (ability as IModifieDamageAbility).MaxDamageModifier(damage, source);

        return damage_modifier;
    }

    public float DamageModifier(int damage, UnitsGroupScript source)
    {
        float damage_modifier = 1;

        foreach (BattleAbility ability in Abilities)
            if (ability is IModifieDamageAbility)
                damage_modifier *= (ability as IModifieDamageAbility).DamageModifier(damage, source);

        return damage_modifier;
    }

    public bool IsShooting
    {
        get
        {
            foreach (BattleAbility ability in Abilities)
                if (ability is IShootAbility) return true;

            return false;
        }
    }

    public void Initialize(BattlegroundScript battleground, UnitsGroup source)
    {
        _battleground = battleground;
        _source_group = source;

        foreach (BattleAbility ability in Source.Unit.Abilities)
            AddAbility(ability.Clone());

        UpdateStatus();
        transform.position = battleground.GetSpacePosition(source.Position);
    }

    public void SetPosition(HexTileScript tile)
    {
        Battleground.Tiles[Position.x][Position.y].RemoveObject();
        tile.SetObject(this);

        Source.SetPosition(tile.Position);
        
    }
    public void SetCount(int count)
    {
        _source_group.SetCount(count);
        UpdateStatus();
    }
    public void SetTeam(BattleTeam team)
    {
        _source_group.SetTeam(team);
        UpdateStatus();
    }
    public void SetHealth(int health)
    {
        _source_group.SetHealth(health);
        UpdateStatus();
    }

    public void TakeDamage(int damage, UnitsGroupScript source)
    {
        Debug.LogWarning("Damage: " + damage);

        int max_health = Source.Count * Source.Unit.MaxHealth + Source.Health;
        int max_damage = Mathf.Min(max_health, damage);

        int killed = max_damage / Source.Unit.MaxHealth;
        SetCount(Source.Count - killed);

        int unit_damage = max_damage - Source.Unit.MaxHealth * killed;
        if(unit_damage > Source.Health)
        {
            SetCount(Source.Count - 1);
            SetHealth(Source.Health - (unit_damage -Source.Health));
        }
        else
        {
            SetHealth(Source.Health - unit_damage);
        }



        Battleground.OnTakeDamage(max_damage, this, source);

        if (Source.Count <= 0)
            Death();

    }
    public void Death()
    {
        Tile.RemoveObject();
        UpdateStatus();
    }

    public void AddAbility(BattleAbility ability)
    {
        ability.SetHandler(this);
        _abilities.Add(ability);
    }
    public void RemoveAbility(BattleAbility ability)
    {
        _abilities.Remove(ability);
    }
    public void UpdateStatus()
    {
        if (_source_group.Count <= 0)
            gameObject.SetActive(false);
        else
            gameObject.SetActive(true);

        if (_source_group.Team == BattleTeam.Player1)
            GetComponent<SpriteRenderer>().color = Color.green;
        else
            GetComponent<SpriteRenderer>().color = Color.red;

        Counter.text = _source_group.Count.ToString() + "(" + _source_group.Health + ")";
        Name.text = Source.Unit.Id;
    }

    public void OnStartTurn(UnitsGroupScript group_turn)
    {
            foreach (BattleAbility ability in Abilities)
                if (ability is IOnStartTurnAbility)
                    (ability as IOnStartTurnAbility).OnStartTurn(group_turn);
    }

    public void TickEffects()
    {
        List<ITickableEffect> tickable_effects = new List<ITickableEffect>();

        foreach (BattleEffect effect in Effects)
            if (effect is ITickableEffect)
                tickable_effects.Add(effect as ITickableEffect);

        foreach (ITickableEffect effect in tickable_effects)
            effect.Tick();
    }

    public void OnTakeDamage(int damage, UnitsGroupScript target, UnitsGroupScript source)
    {
        foreach (BattleAbility ability in Abilities)
            if (ability is IOnTakeDamageAbility)
                (ability as IOnTakeDamageAbility).OnTakeDamage(damage, target, source);
    }


}
